

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import model.User;


@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public loginServlet() {
        super();
       
    }

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		boolean isConnected = false;
		JSONArray list= new JSONArray();
		JSONObject item = new JSONObject();

		
		try {
			String username = request.getParameter("username");
			String password = request.getParameter("pass");
			
			User user= new User();
			isConnected = user.Connect(username,password);
			
			if (isConnected) {
				item.put("connect", true);
			}else {
				item.put("connect", false);
			}
			list.add(item);
			
			response.getWriter().append(list.toJSONString());
		}catch (Exception ex) {
			System.out.println("Error on LoginServlet DoGet:" + ex.getMessage());
		}
		
		
		//.append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
